- 感谢我的同事-CSC,他提出了更好的方案,所以这是个重构的版本.
- 更新如下:

1. 在注释中加入错误返回码。
2. 检查结果包装成为一个更完整的对象,包含返回码和说明（要将这个结果返回给他人,这样包装很有必要）
3. 之前版本使用之前要初始化,以及手动添加anno类型和检查类,方便理解,但实在很不雅观,好了,这次不需要了。

首先按如下说明，你可以自定义一个新注解叫PersonName:
```
public @interface PersonName {
  public String name() default "";
  public int errorCode() default 0;// 错误返回码,随便写不写都可以,自己要用就写
  public String errorMsg() default "姓名验证无法通过";// 错误描述,同上
  public String mapClazz() default "com.beanverify.test.verifyinterface.impl.VerifyPersonName";// 必要的方法，非可选，名称需要和mapClazz保持一致，通过这个方法将PersonName这个注解和VerfiyPersonName这个执行类映射起来，以实现验证行为。
}
```
VerifyPersonName类实现接口 AnnotationVerifier 即可，接口中只有一个doVerify(Object paramValue, String paramName, Anno annotation)方法需要实现。其中 paramValue是被注解字段的属性值，以如下Person类中的age举例，假设我们后面给age赋值为1，那么这个paramValue的值就是1。paramName当然就是age本身的名字。annotation就是@Number这个注解。因此，很显然，当你自己实现这个age头上的@Number注解的时候，@Number对应的判断方法的逻辑内部，你首先要判断的就是这个paramValue是否是一个数字。然后才是其他判断，比如你设定了大小限制，不能为负数之类的，这些实现细节，你可以参考内置验证方法的逻辑代码。

下面是如何使用，你可以定义类似这样一个bean:Person
```
public class Person {
@Null(errorMsg = "姓名不能为空",errorCode = 1)
@PersonName(name="GoodName")
private String name;

@Number(maxSize=1)
private String age;

@Email
private String mailAdress;

@Ip
private String myIp;

@Date(format="yyyyMMddHHmmss")
private String today;

@Null
private Person person;
```

- 然后给它填充数据:

```
Person person = new Person();
person.setName("GoodName");
person.setAge("1");
person.setMyIp("127.0.0.1");
person.setMailAdress("34897@qq.com");
person.setToday("2013-05-06 14:00:00");
person.setPerson(null);
```
开始做验证:

```
VerifyResult result = MyBeanChecker.doCheck(person);
Assert.assertTrue(result.isSuccess());//判断结果
```
OK.完毕！