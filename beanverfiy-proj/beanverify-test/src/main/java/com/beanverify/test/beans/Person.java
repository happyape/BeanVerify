package com.beanverify.test.beans;

import com.beanverify.annotations.Date;
import com.beanverify.annotations.Email;
import com.beanverify.annotations.Ip;
import com.beanverify.annotations.Null;
import com.beanverify.annotations.Number;
import com.beanverify.test.annotation.PersonName;


/**
 * @author KivenWon
 *
 */
public class Person {
	@Null(errorMsg = "姓名不能为空",errorCode = 1)
	@PersonName(name="GoodName")
	private String name;

	@Number(maxSize=1)
	private String age;

	@Email
	private String mailAdress;

	@Ip
	private String myIp;

	@Date(format="yyyyMMddHHmmss")
	private String today;

	@Null
	private Person person;
	public Person(){}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	public String getMailAdress() {
		return mailAdress;
	}
	public void setMailAdress(String mailAdress) {
		this.mailAdress = mailAdress;
	}
	public String getMyIp() {
		return myIp;
	}
	public void setMyIp(String myIp) {
		this.myIp = myIp;
	}
	public String getToday() {
		return today;
	}
	public void setToday(String today) {
		this.today = today;
	}
}