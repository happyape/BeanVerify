package com.beanverify.test.example;

import org.junit.Assert;
import org.junit.Test;

import com.beanverify.inter.impl.MyBeanChecker;
import com.beanverify.test.beans.Person;

public class TestPerson {

	@Test
	public void testPerson(){
		Person person = new Person();
		person.setName("");
		person.setAge("1");
		person.setMyIp("127.0.0.1");
		person.setMailAdress("34897@qq.com");
//		person.setMailAdress(null);
//		person.setToday("20130506140000");
//		person.setToday("kdkfjd");
		person.setToday("2013-05-06 14:00:00");
//		person.setToday("");
//		person.setPerson(person);
		person.setPerson(null);
		MyBeanChecker.doCheck(person).isSuccess();
		Assert.assertTrue(MyBeanChecker.doCheck(person).isSuccess());
	}
}