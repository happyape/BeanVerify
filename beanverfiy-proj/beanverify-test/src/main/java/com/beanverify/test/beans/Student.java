package com.beanverify.test.beans;

import com.beanverify.annotations.Null;
import com.beanverify.annotations.Number;

public class Student {

	@Null(errorMsg = "姓名不能为空",errorCode = 1)
	public String name;
	
	@Null(errorMsg = "年龄不能为空",errorCode = 1)
	@Number(errorMsg = "年龄必须是数字",errorCode = 2)
	public int age;
}
