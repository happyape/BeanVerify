package com.beanverify.test.verifyinterface.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beanverify.exceptions.UnKnownVerifyException;
import com.beanverify.exceptions.UnSupportedParamTypeException;
import com.beanverify.inter.AnnotationVerifier;
import com.beanverify.inter.VerifyResult;
import com.beanverify.inter.impl.MyVerifyResult;
import com.beanverify.test.annotation.PersonName;
import com.beanverify.utils.UtilCommon;

public class VerifyPersonName implements AnnotationVerifier<PersonName>{
	private static final Logger LOG = LoggerFactory.getLogger(VerifyPersonName.class);

	public VerifyResult doVerify(Object param, String paramName, PersonName personNameAnno) throws UnSupportedParamTypeException, UnKnownVerifyException{
		try {
			String name = (String)param;
			if (UtilCommon.isNull(name)){
				LOG.warn(String.format("[%s]参数为空!无法受检PersonName,但默认允许为空,默认可以通过--√",paramName));
				return MyVerifyResult.DEFAULT_SUCCESS;
			}
			if (name.equals(personNameAnno.name())){
				LOG.debug(String.format("[%s]名称一致,检查通过--√",paramName));
				return MyVerifyResult.DEFAULT_SUCCESS;
			} else{
				LOG.debug(String.format("[%s]%s--×",paramName,personNameAnno.errorMsg()));
				return new MyVerifyResult(false, personNameAnno.errorCode(), personNameAnno.errorMsg());
			}
		} catch(ClassCastException e){
			LOG.error("VerifyPersonName参数检查异常",e);
			throw new UnSupportedParamTypeException("VerifyPersonName参数检查异常");
		} catch(Exception e){
			LOG.error("VerifyPersonName参数检查异常",e);
			throw new UnKnownVerifyException("未知的VerifyPersonName异常!");
		}
	}

}