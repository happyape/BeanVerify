package com.beanverify.utils;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UtilCommon {
	public static List<String> serialNos = new ArrayList<String>();
	private static String pattern = "((11|12|13|14|15|21|22|23|31|32|33|34|35|36|37|41|42|43|44|45|46|50|51|52|53|54|61|62|63|64|65|71|81|82|91)\\d{4})((((19|20)(([02468][048])|([13579][26]))0229))|((20[0-9][0-9])|(19[0-9][0-9]))((((0[1-9])|(1[0-2]))((0[1-9])|(1\\d)|(2[0-8])))|((((0[1,3-9])|(1[0-2]))(29|30))|(((0[13578])|(1[02]))31))))((\\d{3}(x|X))|(\\d{4}))";
	public static int uniqueKey = 1;

	public static boolean isMobNO(String mobiles) {
		Pattern p = Pattern.compile("^((13[0-9])|(15[^4,\\D])|(18[0-9])|(147)|(145))\\d{8}$");
		Matcher m = p.matcher(mobiles);
		return m.matches();
	}

	public static boolean isIDNo(String idNumber) {
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(idNumber);
		return m.matches();
	}

	public static boolean isEmail(String email) {
		String str = "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$";
		Pattern p = Pattern.compile(str);
		Matcher m = p.matcher(email);
		return m.matches();
	}

	public static boolean isNull(String... values) {
		for (String string : values) {
			if (string == null || "".equals(string)) {
				return true;
			}
		}
		return false;
	}

	public static boolean isNumber(String source) {
		char[] seq = source.toCharArray();
		for (char c : seq) {
			if (c < 48 || c > 57) {
				return false;
			}
		}
		return true;
	}

	public static boolean containSpaces(String... values) {
		for (String str : values) {
			if (str.contains(" ")) {
				return true;
			}
		}
		return false;
	}

	public static boolean isValidPwd(String pwd) {
		if (pwd == null) {
			return false;
		}
		if (!containSpaces(pwd)) {
			return false;
		}
		if (pwd.length() != 32) {
			return false;
		}
		return true;
	}

	/**
	 * 
	 * @param date
	 * @param format
	 * @since 1.0
	 */
	public static Date fromatDate(String date, String format) {
		Date ddDate = null;
		try {
			SimpleDateFormat sbf = new SimpleDateFormat(format);
			sbf.setLenient(false);
			ddDate = sbf.parse(date);
		} catch (Exception ex) {
		}
		return ddDate;
	}

	public static boolean verifyDateTime(String dateTime, String format) {
		boolean flag = false;

		flag = verifyStr(dateTime);
		if (!flag) {
			return flag;
		}
		flag = verifyStr(format);
		if (!flag) {
			return flag;
		}

		if (dateTime == null || "".equals(dateTime)) {
			return flag;
		}

		Date d = fromatDate(dateTime, format);
		if (d != null) {
			flag = true;
		}
		return flag;
	}

	public static boolean verifyDate(String date) {
		return verifyDateTime(date, "yyyy-MM-dd");
	}

	/**
	 * 
	 * @param str
	 * @since 1.0
	 */
	public static boolean verifyStr(String str) {
		boolean flag = false;
		if (str == null || "".equals(str.trim())) {
			return flag;
		}
		if ("NA".equalsIgnoreCase(str)) {
			return flag;
		}
		flag = true;
		return flag;
	}

	/**
	 * 
	 * @param ip
	 * @return
	 */
	public static boolean isIP(String ip) {
		String regex = "^(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|[1-9])\\." + "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\." + "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\." + "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)$";
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(ip);
		return m.matches();
	}
}
