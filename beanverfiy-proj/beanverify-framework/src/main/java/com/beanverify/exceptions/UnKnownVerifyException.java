package com.beanverify.exceptions;

public class UnKnownVerifyException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UnKnownVerifyException(String msg){
		super(msg);
	}
}