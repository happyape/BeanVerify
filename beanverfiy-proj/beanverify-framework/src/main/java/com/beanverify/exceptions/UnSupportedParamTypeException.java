package com.beanverify.exceptions;

public class UnSupportedParamTypeException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UnSupportedParamTypeException(String msg){
		super(msg);
	}

}
