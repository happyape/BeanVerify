package com.beanverify.inter.verifier.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beanverify.annotations.Email;
import com.beanverify.exceptions.UnKnownVerifyException;
import com.beanverify.exceptions.UnSupportedParamTypeException;
import com.beanverify.inter.AnnotationVerifier;
import com.beanverify.inter.VerifyResult;
import com.beanverify.inter.impl.MyVerifyResult;
import com.beanverify.utils.UtilCommon;

public class VerifyEmail implements AnnotationVerifier<Email>{
	private static final Logger LOG = LoggerFactory.getLogger(VerifyEmail.class);

	public VerifyResult doVerify(Object paramValue, String paramName, Email emailAnno) throws UnSupportedParamTypeException, UnKnownVerifyException {
		try {
			String email = (String)paramValue;
			if (UtilCommon.isNull(email)){
				LOG.warn(String.format("[%s]默认允许为空,可以通过--√",paramName));
				return MyVerifyResult.DEFAULT_SUCCESS;
			}
			if (!UtilCommon.isEmail(email)){
				LOG.debug(String.format("[%s]%s--×",paramName,emailAnno.errorMsg()));
				return new MyVerifyResult(false, emailAnno.errorCode(), emailAnno.errorMsg());
			} else{
				LOG.debug(String.format("[%s]Email检查通过--√",paramName));
				return MyVerifyResult.DEFAULT_SUCCESS;
			}
		} catch(ClassCastException e){
			LOG.error("VerifyEmail异常!",e);
			throw new UnSupportedParamTypeException("VerifyEmail异常!");
		} catch(Exception e){
			LOG.error("VerifyEmail异常!",e);
			throw new UnKnownVerifyException("未知的VerifyEmail异常!");
		}
	}

}