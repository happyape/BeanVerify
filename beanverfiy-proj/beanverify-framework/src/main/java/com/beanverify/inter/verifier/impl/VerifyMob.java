package com.beanverify.inter.verifier.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beanverify.annotations.Mob;
import com.beanverify.exceptions.UnKnownVerifyException;
import com.beanverify.exceptions.UnSupportedParamTypeException;
import com.beanverify.inter.AnnotationVerifier;
import com.beanverify.inter.VerifyResult;
import com.beanverify.inter.impl.MyVerifyResult;
import com.beanverify.utils.UtilCommon;



public class VerifyMob implements AnnotationVerifier<Mob>{
	private static final Logger LOG = LoggerFactory.getLogger(VerifyMob.class);

	public VerifyResult doVerify(Object paramValue, String paramName, Mob mobAnno)  throws UnSupportedParamTypeException, UnKnownVerifyException{
		try {
			String mob = (String)paramValue;
			if (UtilCommon.isNull(mob)){
				LOG.warn(String.format("[%s]默认允许为空,可以通过--√",paramName));
				return MyVerifyResult.DEFAULT_SUCCESS;
			}
			if (!UtilCommon.isMobNO(mob)){
				LOG.debug(String.format("[%s]%s--×",paramName,mobAnno.errorMsg()));
				return new MyVerifyResult(false, mobAnno.errorCode(), mobAnno.errorMsg());
			} else {
				LOG.debug(String.format("[%s]Mob检查通过--√",paramName));
				return MyVerifyResult.DEFAULT_SUCCESS;
			}
		} catch(ClassCastException e){
			LOG.error("VerifyMob异常!",e);
			throw new UnSupportedParamTypeException("VerifyMob异常!");
		} catch(Exception e){
			LOG.error("未知的VerifyMob异常!",e);
			throw new UnKnownVerifyException("未知的VerifyMob异常!");
		}
	}
}