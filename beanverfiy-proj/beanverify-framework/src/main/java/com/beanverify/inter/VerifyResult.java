package com.beanverify.inter;

public interface VerifyResult {
	boolean isSuccess();
	String getResultDesc();
	int getResultCode();
}