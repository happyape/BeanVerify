package com.beanverify.inter.verifier.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beanverify.annotations.Null;
import com.beanverify.inter.AnnotationVerifier;
import com.beanverify.inter.VerifyResult;
import com.beanverify.inter.impl.MyVerifyResult;



public class VerifyNull implements AnnotationVerifier<Null>{
	private static final Logger LOG = LoggerFactory.getLogger(VerifyNull.class);

	public VerifyResult doVerify(Object param, String paramName, Null nullAnno) {
		if (param == null || "".equals(param)) {
			LOG.debug(String.format("[%s]%s--×",paramName,nullAnno.errorMsg()));
			return new MyVerifyResult(false, nullAnno.errorCode(), nullAnno.errorMsg());
		}
		LOG.debug(String.format("[%s]参数不为空--√",paramName));
		return MyVerifyResult.DEFAULT_SUCCESS;
	}
}