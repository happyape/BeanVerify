package com.beanverify.inter.verifier.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beanverify.annotations.Ip;
import com.beanverify.exceptions.UnKnownVerifyException;
import com.beanverify.exceptions.UnSupportedParamTypeException;
import com.beanverify.inter.AnnotationVerifier;
import com.beanverify.inter.VerifyResult;
import com.beanverify.inter.impl.MyVerifyResult;
import com.beanverify.utils.UtilCommon;



public class VerifyIp implements AnnotationVerifier<Ip>{
	private static final Logger LOG = LoggerFactory.getLogger(VerifyIp.class);

	public VerifyResult doVerify(Object paramValue, String paramName, Ip ipAnno) throws UnSupportedParamTypeException, UnKnownVerifyException {
		try {
			String ip = (String)paramValue;
			if (UtilCommon.isNull(ip)){
				LOG.warn(String.format("[%s]默认允许为空,可以通过--√",paramName));
				return MyVerifyResult.DEFAULT_SUCCESS;
			}
			if (!UtilCommon.isIP(ip)){
				LOG.debug(String.format("[%s]%s--×",paramName,ipAnno.errorMsg()));
				return new MyVerifyResult(false, ipAnno.errorCode(), ipAnno.errorMsg());
			} else {
				LOG.debug(String.format("[%s]Ip检查通过--√",paramName));
				return MyVerifyResult.DEFAULT_SUCCESS;
			}
		} catch(ClassCastException e){
			LOG.error("VerifyIp异常!",e);
			throw new UnSupportedParamTypeException("VerifyIp异常!");
		} catch(Exception e){
			LOG.error("未知的VerifyIp异常!",e);
			throw new UnKnownVerifyException("未知的VerifyIp异常!");
		}
	}
}