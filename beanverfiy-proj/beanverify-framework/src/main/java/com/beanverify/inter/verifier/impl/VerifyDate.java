package com.beanverify.inter.verifier.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beanverify.annotations.Date;
import com.beanverify.exceptions.UnKnownVerifyException;
import com.beanverify.exceptions.UnSupportedParamTypeException;
import com.beanverify.inter.AnnotationVerifier;
import com.beanverify.inter.VerifyResult;
import com.beanverify.inter.impl.MyVerifyResult;
import com.beanverify.utils.UtilCommon;



public class VerifyDate implements AnnotationVerifier<Date>{
	private static final Logger LOG = LoggerFactory.getLogger(VerifyDate.class);

	public VerifyResult doVerify(Object paramValue, String paramName, Date dateAnno) throws UnSupportedParamTypeException, UnKnownVerifyException {
		try {
			String date = (String)paramValue;
			if (UtilCommon.isNull(date)){
				LOG.warn(String.format("[%s]参数为空!无法受检Date,但默认允许为空,可以通过--√",paramName));
				return MyVerifyResult.DEFAULT_SUCCESS;
			}
			java.util.Date theDate = UtilCommon.fromatDate(date, dateAnno.format());
			if (theDate == null){
				LOG.debug(String.format("[%s]%s--×",paramName, dateAnno.errorMsg()));
				return new MyVerifyResult(false, dateAnno.errorCode(), dateAnno.errorMsg());
			} else {
				LOG.debug(String.format("[%s]Date检查通过--√",paramName));
				return MyVerifyResult.DEFAULT_SUCCESS;
			}
		} catch(ClassCastException e){
			LOG.error("VerifyDate异常!",e);
			throw new UnSupportedParamTypeException("VerifyDate异常!");
		} catch(Exception e){
			LOG.error("未知的VerifyDate异常!",e);
			throw new UnKnownVerifyException("未知的VerifyDate异常!");
		}
	}
}