package com.beanverify.inter;

import java.lang.annotation.Annotation;

import com.beanverify.exceptions.UnKnownVerifyException;
import com.beanverify.exceptions.UnSupportedParamTypeException;

public interface AnnotationVerifier<Anno extends Annotation> {
	/**
	 * 
	 * @param paramValue 属性值
	 * @param paramName 属性名称
	 * @param annotation 属性对应的注解
	 * @return
	 */
	VerifyResult doVerify(Object paramValue, String paramName, Anno annotation) throws UnSupportedParamTypeException,UnKnownVerifyException;
}