package com.beanverify.inter;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;


/**
 * 
 * @author KivenWon
 */
public interface AnnotationBox {

	@SuppressWarnings("rawtypes")
	public static Map<Class<? extends Annotation>, AnnotationVerifier> verifyMap = new HashMap<Class<? extends Annotation>, AnnotationVerifier>();

	void addNewAnno(Class<? extends Annotation> annoclazz,@SuppressWarnings("rawtypes") Class<? extends AnnotationVerifier> annoVerifyClazz);
	AnnotationVerifier<?> getAnno(Class<? extends Annotation> annoclazz);
}
