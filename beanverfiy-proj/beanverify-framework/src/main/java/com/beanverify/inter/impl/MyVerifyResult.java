package com.beanverify.inter.impl;

import com.beanverify.inter.VerifyResult;

public class MyVerifyResult implements VerifyResult{
	public static MyVerifyResult DEFAULT_SUCCESS = new MyVerifyResult(true,1,"success");
	public static MyVerifyResult DEFAULT_FAILED = new MyVerifyResult(false,0,"failed");
	private boolean isSucess;
	private int resultCode;
	private String resultDesc;

	public MyVerifyResult(){}

	public MyVerifyResult(boolean isSucess, int resultCode, String resultDesc){
		this.isSucess = isSucess;
		this.resultCode = resultCode;
		this.resultDesc = resultDesc;
	}
	
	public boolean isSuccess() {
		return isSucess;
	}

	public String getResultDesc() {
		return resultDesc;
	}

	public int getResultCode() {
		return resultCode;
	}

	public void setResult(boolean isSucess, int resultCode, String resultDesc) {
		this.isSucess = isSucess;
		this.resultCode = resultCode;
		this.resultDesc = resultDesc;
	}

}