package com.beanverify.inter;

public interface BeanChecker{

	VerifyResult doCheckBean(Object bean);
}