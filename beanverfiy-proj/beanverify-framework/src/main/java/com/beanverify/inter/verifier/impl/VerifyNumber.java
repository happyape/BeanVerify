package com.beanverify.inter.verifier.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beanverify.annotations.Number;
import com.beanverify.exceptions.UnKnownVerifyException;
import com.beanverify.exceptions.UnSupportedParamTypeException;
import com.beanverify.inter.AnnotationVerifier;
import com.beanverify.inter.VerifyResult;
import com.beanverify.inter.impl.MyVerifyResult;
import com.beanverify.utils.UtilCommon;

public class VerifyNumber implements AnnotationVerifier<Number>{
	private static final Logger LOG = LoggerFactory.getLogger(VerifyNumber.class);

	public VerifyResult doVerify(Object param, String paramName, Number numAnno) throws UnSupportedParamTypeException, UnKnownVerifyException{
		try {
			String num = (String)param;
			if (UtilCommon.isNull(num)){
				LOG.warn(String.format("[%s]默认允许为空,默认可以通过--√",paramName));
				return MyVerifyResult.DEFAULT_SUCCESS;
			}
			if (UtilCommon.isNumber(num)){
				LOG.debug(String.format("[%s]都是由数字组成,检查通过--√",paramName));
			} else{
				LOG.debug(String.format("[%s]中参杂了非数字,检查失败--×",paramName));
				return new MyVerifyResult(false, numAnno.errorCode(), "参杂了非数字");
			}
			if (num.length() > numAnno.maxSize()){
				LOG.debug(String.format("[%s]超长,检查失败--×",paramName));
				return new MyVerifyResult(false, numAnno.errorCode(), "超长");
			} else{
				LOG.debug(String.format("[%s]长度在合法范围内,检查通过--√",paramName));
				return MyVerifyResult.DEFAULT_SUCCESS;
			}
		} catch(ClassCastException e){
			LOG.error("VerifyNumber参数检查异常",e);
			throw new UnSupportedParamTypeException("VerifyNumber参数检查异常");
		} catch(Exception e){
			LOG.error("VerifyNumber参数检查异常",e);
			throw new UnKnownVerifyException("未知的VerifyNumber异常!");
		}
	}
}
