package com.beanverify.inter.impl;

import java.lang.annotation.Annotation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beanverify.inter.AnnotationBox;
import com.beanverify.inter.AnnotationVerifier;

public class MyAnnotationBox implements AnnotationBox{
	private static Logger log = LoggerFactory.getLogger(MyAnnotationBox.class);

	@SuppressWarnings("rawtypes")
	public void addNewAnno(Class<? extends Annotation> annoclazz, Class<? extends AnnotationVerifier> annoVerifyClazz) {
		try {
			verifyMap.put(annoclazz, annoVerifyClazz.newInstance());
		} catch (InstantiationException e) {
			log.error("BeanVerifyError",e);
		} catch (IllegalAccessException e) {
			log.error("BeanVerifyError",e);
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public AnnotationVerifier getAnno(Class<? extends Annotation> annoclazz) {
		return verifyMap.get(annoclazz);
	}

}