package com.beanverify.annotations;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Ip {
	public boolean cantNull() default true;
	// 错误返回码
	public int errorCode() default 0;
	// 错误描述
	public String errorMsg() default "IP检查失败";
	// 验证实现类
	public String mapClazz() default "com.beanverify.inter.verifier.impl.VerifyIp";
}
