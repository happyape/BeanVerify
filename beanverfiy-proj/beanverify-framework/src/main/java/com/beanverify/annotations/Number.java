package com.beanverify.annotations;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Number {
	public int maxSize() default 20;
	// 错误返回码
	public int errorCode() default 0;
	// 错误描述
	public String errorMsg() default "不是数字";
	// 验证实现类
	public String mapClazz() default "com.beanverify.inter.verifier.impl.VerifyNumber";
}